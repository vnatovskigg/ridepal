import { useState } from 'react';
import { Route, Switch, useLocation } from 'react-router-dom';
import './App.css';
import Navbar from './components/Navbar/Navbar';
import AuthContext, { getUser } from './providers/authentication';
import Home from './components/Home/Home';
import Login from './components/Login/Login';
import Register from './components/Register/Register';
import UserProfile from './components/UserProfile/UserProfile';
import Playlist from './components/Playlist/Playlist';
import Error404 from './components/Errors/Error404/Error404';
import GuardedRoute from './components/GuardedRoute/GuardedRoute';
import { createMuiTheme, ThemeProvider } from '@material-ui/core';
import { blueGrey, deepPurple } from '@material-ui/core/colors';
import AdminPanel from './components/AdminPanel/AdminPanel';
import Routes from './components/Routes/Routes';
import Genres from './components/Genres/Genres';
import { AnimatePresence } from 'framer-motion';
import ChangePassword from './components/ChangePassword/ChangePassword';
import Explore from './components/Explore/Explore';

function App() {
	const location = useLocation();
	const [authValue, setAuthState] = useState({
		user: getUser(),
		isLoggedIn: Boolean(getUser()),
	});
	const [darkTheme, setDarkTheme] = useState(false);

	const toggleDarkTheme = () => {
		setDarkTheme((prev) => !prev);
	};

	const theme = createMuiTheme({
		palette: {
			primary: darkTheme
				? deepPurple
				: {
						light: '#e1f3f4',
						main: '#fff',
						dark: '#b9e7ea',
				  },
			secondary: darkTheme
				? blueGrey
				: {
						light: '#e1f3f4',
						main: '#3A8DA6',
						dark: '#b9e7ea',
				  },
			type: darkTheme ? 'dark' : 'light',
		},
		typography: {
			fontFamily: 'Quicksand',
		},
	});

	return (
		<ThemeProvider theme={theme}>
			<div className={darkTheme ? 'dark-theme' : 'light-theme'}>
				<div className={darkTheme ? 'dark-theme-background' : 'light-theme-background'} />
				<AuthContext.Provider value={{ ...authValue, setAuthState: setAuthState }}>
					<Navbar darkTheme={darkTheme} toggleDarkTheme={toggleDarkTheme} />
					<AnimatePresence exitBeforeEnter>
						<Switch location={location} key={location.key}>
							<Route path='/' exact to component={() => <Home darkTheme={darkTheme} />} />
							<GuardedRoute path='/login' isLoggedIn={!authValue?.isLoggedIn} darkTheme={darkTheme} component={Login} />
							<GuardedRoute path='/register' isLoggedIn={!authValue?.isLoggedIn} darkTheme={darkTheme} component={Register} />
							<GuardedRoute path='/profile/:id' isLoggedIn={authValue?.isLoggedIn} darkTheme={darkTheme} component={UserProfile} />
							<GuardedRoute path='/change-password' isLoggedIn={authValue?.isLoggedIn} darkTheme={darkTheme} component={ChangePassword} />
							<GuardedRoute path='/:id/playlists/:pid' isLoggedIn={authValue?.isLoggedIn} darkTheme={darkTheme} component={Playlist} />
							<GuardedRoute
								path='/admin-panel'
								isLoggedIn={authValue?.isLoggedIn && authValue?.user?.role === 'Admin'}
								darkTheme={darkTheme}
								component={AdminPanel}
							/>
							<GuardedRoute path='/generate/routes' isLoggedIn={authValue?.isLoggedIn} darkTheme={darkTheme} component={Routes} />
							<GuardedRoute path='/generate/genres' isLoggedIn={authValue?.isLoggedIn} darkTheme={darkTheme} component={Genres} />
							<GuardedRoute path='/generate/playlist' isLoggedIn={authValue?.isLoggedIn} darkTheme={darkTheme} component={Playlist} />
							<GuardedRoute path='/explore' isLoggedIn={authValue?.isLoggedIn} darkTheme={darkTheme} component={Explore} />
							<Route path='*' component={Error404} />
						</Switch>
					</AnimatePresence>
				</AuthContext.Provider>
			</div>
		</ThemeProvider>
	);
}

export default App;
