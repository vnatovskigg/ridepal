export const registerUser = async ({ email, username, password }) => {
	try {
		const res = await fetch(`http://localhost:${process.env.REACT_APP_BEPORT}/v1/api/register`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({ email, username, password }),
		});

		return await res.json();
	} catch (err) {
		console.warn(err.message);
	}
};

export const loginUser = async (loginDetails) => {
	const res = await fetch(`http://localhost:${process.env.REACT_APP_BEPORT}/v1/api/login`, {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
		},
		body: JSON.stringify(loginDetails),
	});

	return await res.json();
};

export const logoutUser = async (token) => {
	const res = await fetch(`http://localhost:${process.env.REACT_APP_BEPORT}/v1/api/logout`, {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
			Authorization: `Bearer ${token}`,
		},
	});
	const logout = await res.json();

	return logout.message.includes('successful');
};

export const getUserData = async (id, token) => {
	const user = await fetch(`http://localhost:${process.env.REACT_APP_BEPORT}/v1/api/users/${id}`, {
		headers: {
			Authorization: `Bearer ${token}`,
		},
	});

	return await user.json();
};

export const getAllPlaylists = async (token, genres) => {
	const genresArr = Array.from(genres);
	const res = await fetch(
		`http://localhost:${process.env.REACT_APP_BEPORT}/v1/api/users/playlists/all${genresArr.length ? `?genres=${genresArr.join(',')}` : ''}`,
		{
			headers: {
				Authorization: `Bearer ${token}`,
			},
		}
	);

	return await res.json();
};

export const getUserPlaylists = async (id, token, pid = null) => {
	const res = await fetch(`http://localhost:${process.env.REACT_APP_BEPORT}/v1/api/users/${id}/playlists${pid ? `?pid=${pid}` : ''}`, {
		headers: {
			Authorization: `Bearer ${token}`,
		},
	});
	return await res.json();
};

export const updatePlaylist = async (newName, userId, playlistId, token) => {
	const res = await fetch(`http://localhost:${process.env.REACT_APP_BEPORT}/v1/api/users/${userId}/playlists/${playlistId}`, {
		method: 'PUT',
		headers: {
			'Content-Type': 'application/json',
			Authorization: `Bearer ${token}`,
		},
		body: JSON.stringify({ name: newName }),
	});

	return await res.json();
};

export const createPlaylist = async (name, userId, playtime, trackList, genres, poster, token) => {
	const trackIds = trackList.map((track) => track.trackDeezerId).join(',');
	const genreIds = genres.map((genre) => genre.deezerId).join(',');

	const res = await fetch(`http://localhost:${process.env.REACT_APP_BEPORT}/v1/api/users/${userId}/playlists`, {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
			Authorization: `Bearer ${token}`,
		},
		body: JSON.stringify({ name, tracks: trackIds, playtime, genres: genreIds, poster }),
	});

	return await res.json();
};

export const deletePlaylist = async (uid, pid, token) => {
	const res = await fetch(`http://localhost:${process.env.REACT_APP_BEPORT}/v1/api/users/${uid}/playlists/${pid}`, {
		method: 'DELETE',
		headers: {
			Authorization: `Bearer ${token}`,
		},
	});

	return await res.json();
};

export const uploadAvatar = async (id, token, data) => {
	try {
		const res = await fetch(`http://localhost:${process.env.REACT_APP_BEPORT}/v1/api/users/${id}/uploads`, {
			method: 'POST',
			headers: {
				Authorization: `Bearer ${token}`,
			},
			body: data,
		});
		const updatedUser = await res.json();
		return updatedUser.avatar || null;
	} catch {
		return null;
	}
};

export const getRandomImage = async (q) => {
	const randomPage = Math.floor(Math.random() * 10) + 1;
	const res = await fetch(`https://pixabay.com/api/?key=${process.env.REACT_APP_PIXABEY_API}&q=${q}&page=${randomPage}&per_page=50`);
	const images = (await res.json()).hits;

	const randomIndex = Math.floor(Math.random() * images.length);

	return images[randomIndex];
};

export const getTrackData = async (id, token) => {
	const res = await fetch(`http://localhost:${process.env.REACT_APP_BEPORT}/v1/api/tracks/${id}`, {
		headers: {
			Authorization: `Bearer ${token}`,
		},
	});

	return await res.json();
};

export const getAllUsers = async (token) => {
	const res = await fetch(`http://localhost:${process.env.REACT_APP_BEPORT}/v1/api/admin/users`, {
		headers: {
			Authorization: `Bearer ${token}`,
		},
	});

	return await res.json();
};

export const searchAllUsers = async (keyword, token) => {
	const res = await fetch(`http://localhost:${process.env.REACT_APP_BEPORT}/v1/api/admin/users?username=${keyword}`, {
		headers: {
			Authorization: `Bearer ${token}`,
		},
	});

	return await res.json();
};

export const getTracksByGenre = async (genre, token) => {
	const res = await fetch(`http://localhost:${process.env.REACT_APP_BEPORT}/v1/api/tracks?genre=${genre}&pageSize=1000`, {
		headers: {
			Authorization: `Bearer ${token}`,
		},
	});

	return await res.json();
};

export const banUser = async (id, token) => {
	const res = await fetch(`http://localhost:${process.env.REACT_APP_BEPORT}/v1/api/admin/users/banstatus/${id}`, {
		method: 'PUT',
		headers: {
			Authorization: `Bearer ${token}`,
		},
	});

	return await res.json();
};

export const getAllTracks = async (token) => {
	const res = await fetch(`http://localhost:${process.env.REACT_APP_BEPORT}/v1/api/tracks?pageSize=5000`, {
		headers: {
			Authorization: `Bearer ${token}`,
		},
	});

	return await res.json();
};

export const getLocationSuggestions = async (q) => {
	const query = q.split(' ').join('%20');

	if (query.length > 3) {
		const res = await fetch(
			`http://dev.virtualearth.net/REST/v1/Autosuggest?query=${query}&userCircularMapView=42.7339,25.4858,1000000&maxResults=3&includeEntityTypes=Place,Address&key=${process.env.REACT_APP_BING_KEY}`
		);
		const data = await res.json();
		return data.resourceSets[0].resources[0].value;
	}
};

export const getTripDuration = async (x, y) => {
	const pointA = x
		.split(', ')
		.map((val) => val.split(' ').join('%20'))
		.join('%2C');
	const pointB = y
		.split(', ')
		.map((val) => val.split(' ').join('%20'))
		.join('%2C');

	const res = await fetch(
		`http://dev.virtualearth.net/REST/V1/Routes/Driving?wp.0=${pointA}&wp.1=${pointB}&avoid=minimizeTolls&key=${process.env.REACT_APP_BING_KEY}`
	);
	const data = await res.json();
	return data.resourceSets[0]?.resources[0]?.travelDurationTraffic || null;
};

export const getGenresInfo = async (token) => {
	const res = await fetch(`http://localhost:${process.env.REACT_APP_BEPORT}/v1/api/tracks/genres`, {
		headers: {
			Authorization: `Bearer ${token}`,
		},
	});
	const genresData = await res.json();
	return genresData;
};

export const updateUser = async (id, token, data) => {
	try {
		const res = await fetch(`http://localhost:${process.env.REACT_APP_BEPORT}/v1/api/admin/users/${id}`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${token}`,
			},
			body: JSON.stringify(data),
		});
		const updatedUser = await res.json();
		return updatedUser || null;
	} catch {
		return null;
	}
};

export const updateUserPassword = async (token, password) => {
	const res = await fetch(`http://localhost:${process.env.REACT_APP_BEPORT}/v1/api/users`, {
		method: 'PUT',
		headers: {
			'Content-Type': 'application/json',
			Authorization: `Bearer ${token}`,
		},
		body: JSON.stringify({ password }),
	});
	const updatedUser = await res.json();
	return updatedUser || null;
};

// export const updatePlaylist = async (newName, userId, playlistId, token) => {
// 	const res = await fetch(`http://localhost:${process.env.REACT_APP_BEPORT}/v1/api/users/${userId}/playlists/${playlistId}`, {
// 		method: 'PUT',
// 		headers: {
// 			'Content-Type': 'application/json',
// 			Authorization: `Bearer ${token}`,
// 		},
// 		body: JSON.stringify({ name: newName }),
// 	});

// 	return await res.json();
// };

// export const createPlaylist = async (name, userId, playtime, trackList, token) => {
// 	const trackIds = trackList.map((track) => track.trackDeezerId).join(',');
// 	const res = await fetch(`http://localhost:${process.env.REACT_APP_BEPORT}/v1/api/users/${userId}/playlists`, {
// 		method: 'POST',
// 		headers: {
// 			'Content-Type': 'application/json',
// 			Authorization: `Bearer ${token}`,
// 		},
// 		body: JSON.stringify({ name: name, tracks: trackIds, playtime: playtime }),
// 	});

// 	return await res.json();
// };

// export const deletePlaylist = async (uid, pid, token) => {
// 	const res = await fetch(`http://localhost:${process.env.REACT_APP_BEPORT}/v1/api/users/${uid}/playlists/${pid}`, {
// 		method: 'DELETE',
// 		headers: {
// 			Authorization: `Bearer ${token}`,
// 		},
// 	});

// 	return await res.json();
// };
