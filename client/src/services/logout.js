import { logoutUser } from '../providers/requests';

/**
 * Logs out a user
 * @param {string} token
 * @param {object} auth
 */
export const logout = (token, auth) => {
	logoutUser(token);
	localStorage.removeItem('token');
	auth.setAuthState({
		user: null,
		isLoggedIn: false,
	});
};
