import { getAllTracks, getTracksByGenre } from '../providers/requests.js';

const getTracks = async (genres, token) => {
	const tracks = genres.length
		? await Promise.all(
				genres.map(async (genre) => {
					return { genre: genre.name, tracks: await getTracksByGenre(genre.name.toLowerCase(), token) };
				})
		  )
		: await getAllTracks(token);

	return tracks;
};

export default getTracks;
