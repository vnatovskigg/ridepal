const convert = (playtime) => {
	const timeInMinutes = playtime / 60;

	if (timeInMinutes < 60) {
		const seconds = playtime % 60 < 10 ? `${playtime % 60}0` : playtime % 60;
		return `${Math.floor(timeInMinutes)}:${seconds}`;
	} else {
		const timeInHours = timeInMinutes / 60;
		let [hours, minutes] = timeInHours.toFixed(2).split('.');

		if (minutes >= 60) {
			hours++;
			minutes = minutes % 60;
		}
        
		return `${hours} hr ${minutes} min`;
	}
};

export default convert;
