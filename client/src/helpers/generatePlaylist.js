import shuffle from './shuffle.js';

const generatePlaylist = (tracks, duration, genres) => {
	const buffer = 300; // 5 minute buffer
	const getRandomIndex = (limit) => {
		return Math.floor(Math.random() * limit);
	};
	

	const populatePlaylist = (remainingDuration = duration, playlist = [], playlistDuration = 0, withGenres = genres.length) => {
		if (remainingDuration < 150) return { playlist: shuffle(playlist), totalPlaytime: playlistDuration };
		if (withGenres) {
			tracks.forEach((genre) => {
				const index = getRandomIndex(genre.tracks.length);
				const track = genre.tracks[index];

				if (track.duration >= remainingDuration + buffer) {
					return populatePlaylist(remainingDuration, playlist, playlistDuration, withGenres);
				} else {
					playlist.push(track);
					remainingDuration -= track.duration;
					playlistDuration += track.duration;
				}
			});
		} else {
			const index = getRandomIndex(tracks.length);
			const track = tracks[index];

			if (track.duration >= remainingDuration + buffer) {
				return populatePlaylist(remainingDuration, playlist, playlistDuration, withGenres);
			} else {
				playlist.push(track);
				remainingDuration -= track.duration;
				playlistDuration += track.duration;
			}
		}

		return populatePlaylist(remainingDuration, playlist, playlistDuration, withGenres);
	};
	return populatePlaylist();
};

export default generatePlaylist;
