import { NavLink, useHistory } from 'react-router-dom';
import { useCallback, useContext, useEffect, useState } from 'react';
import AuthContext, { getToken } from '../../providers/authentication';
import './Navbar.css';
import { Switch } from '@material-ui/core';
import ProfileNav from '../ProfileNav/ProfileNav';
import Logo from '../Logo/Logo';
import SessionExpired from '../Modals/SessionExpired/SessionExpired';
import jwtDecode from 'jwt-decode';
import { logout } from '../../services/logout';
import NavbarMobile from '../NavbarMobile/NavbarMobile';
import { FiMenu } from 'react-icons/fi';
import { IoCloseSharp } from 'react-icons/io5';

const Navbar = ({ toggleDarkTheme, darkTheme }) => {
	const auth = useContext(AuthContext);
	const history = useHistory();
	const [sessionExpiredAlert, setSessionExpiredAlert] = useState(false);
	const token = getToken();
	const [openMobileNav, setOpenMobileNav] = useState(false);

	const toggleMobileMenu = () => setOpenMobileNav(!openMobileNav);

	const closeSessionExpiredAlert = () => setSessionExpiredAlert(false);

	const logoutAction = useCallback(() => {
		logout(token, auth);
		history.push('/');
	}, [auth, history, token]);

	useEffect(() => {
		if (token) {
			const decodedToken = jwtDecode(token);
			const handler = setTimeout(() => {
				setSessionExpiredAlert(true);
				logoutAction();
			}, decodedToken.exp * 1000 - new Date().getTime());

			return () => {
				clearTimeout(handler);
			};
		}
	}, [token, logoutAction]);

	return (
		<>
			{sessionExpiredAlert && <SessionExpired open={sessionExpiredAlert} handleClose={closeSessionExpiredAlert} />}
			{openMobileNav && (
				<NavbarMobile darkTheme={darkTheme} toggleDarkTheme={toggleDarkTheme} toggleMobileMenu={toggleMobileMenu} logout={logoutAction} />
			)}
			<div className='navbar'>
				<Logo darkTheme={darkTheme} />
				<div className='navbar-links'>
					{auth.isLoggedIn ? (
						<>
							<NavLink
								className={darkTheme ? 'nav-link-dark' : 'nav-link-light'}
								activeClassName={darkTheme ? 'active-link-dark' : 'active-link-light'}
								to={'/explore'}
							>
								EXPLORE
							</NavLink>
							<ProfileNav darkTheme={darkTheme} logout={logoutAction} />
						</>
					) : (
						<>
							<NavLink
								className={darkTheme ? 'nav-link-dark' : 'nav-link-light'}
								activeClassName={darkTheme ? 'active-link-dark' : 'active-link-light'}
								to={'/login'}
							>
								LOGIN
							</NavLink>
							<NavLink
								className={darkTheme ? 'nav-link-dark' : 'nav-link-light'}
								activeClassName={darkTheme ? 'active-link-dark' : 'active-link-light'}
								to={'/register'}
							>
								REGISTER
							</NavLink>
						</>
					)}
					<div className={darkTheme ? 'nav-link-dark' : 'nav-link-light'}>
						<Switch checked={darkTheme} onChange={toggleDarkTheme} />
					</div>
				</div>
				<div className='mobile-nav-button' tabIndex={0} role='button' onKeyPress={() => toggleMobileMenu()} onClick={() => toggleMobileMenu()}>
					{openMobileNav ? <IoCloseSharp /> : <FiMenu />}
				</div>
			</div>
			<hr className={darkTheme ? 'hr-dark' : 'hr-light'} />
		</>
	);
};

export default Navbar;
