import RegisterForm from '../Forms/RegisterForm/RegisterForm.js';
import './Register.css';
import { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { Container } from '@material-ui/core';
import { registerUser } from '../../providers/requests.js';
import image from '../../assets/pattern_clouds_web.png';
import imageDark from '../../assets/pattern_clouds_web_dark.png';
import { motion } from 'framer-motion';

const Register = ({ darkTheme }) => {
	const [msg, setMsg] = useState('');
	const history = useHistory();

	const register = async ({ email, username, password }) => {
		const message = await registerUser({ email, username, password });

		if (message.message) {
			setMsg(message.message);
		} else {
			setMsg('Success... redirecting');
			setTimeout(() => {
				history.push('/login');
			}, 2000);
		}
	};

	return (
		<div style={{ flexGrow: 1 }}>
			<Container maxWidth='xl' className='register-root'>
				<motion.div
					className='register-container'
					transition={{ delay: 0.3, duration: 3, type: 'spring', stiffness: 30 }}
					initial={{ x: '-100vw', opacity: 0.75 }}
					animate={{ opacity: 1, x: 0 }}
				>
					<h1 style={{ textAlign: 'center' }}>Register</h1>
					<RegisterForm msg={msg} darkTheme={darkTheme} registerHandler={register} />
				</motion.div>
			</Container>
			<motion.img
				src={darkTheme ? imageDark : image}
				style={{ position: 'fixed', bottom: 0, right: 0, width: '100%', zIndex: -1 }}
				alt='cloud background'
				initial={{ x: 25, y: 50 }}
				animate={{ x: 0, y: 0 }}
				transition={{ duration: 1.5 }}
			></motion.img>
		</div>
	);
};

export default Register;
