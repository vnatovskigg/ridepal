import { Container } from '@material-ui/core';
import React, { useState, useContext } from 'react';
import { useHistory } from 'react-router-dom';
import AuthContext, { createSession } from '../../providers/authentication';
import { loginUser } from '../../providers/requests.js';
import LoginForm from '../Forms/LoginForm/LoginForm';
import image from '../../assets/pattern_clouds_web.png';
import imageDark from '../../assets/pattern_clouds_web_dark.png';
import './Login.css';
import { motion } from 'framer-motion';

const Login = ({ darkTheme }) => {
	const history = useHistory();
	const [msg, setMsg] = useState('');
	const auth = useContext(AuthContext);

	const login = async (user) => {
		const { message, token } = await loginUser(user);

		if (!token) {
			setMsg(message);
		} else {
			setMsg('Welcome!');
			setTimeout(() => {
				createSession(token, auth);
				history.push('/');
			}, 2000);
		}
	};

	return (
		<div style={{ flexGrow: 1 }}>
			<Container maxWidth='xl' className='login-root'>
				<motion.div
					className='login-container'
					transition={{ delay: 0.3, duration: 3, type: 'spring', stiffness: 30 }}
					initial={{ x: '-100vw', opacity: 0.75 }}
					animate={{ opacity: 1, x: 0 }}
					style={{ zIndex: 1 }}
				>
					<h1 style={{ textAlign: 'center' }}>Login</h1>
					<LoginForm msg={msg} darkTheme={darkTheme} loginHandler={login} />
				</motion.div>
			</Container>
			<motion.img
				src={darkTheme ? imageDark : image}
				style={{ position: 'fixed', bottom: 0, right: 0, width: '100%', zIndex: -1 }}
				alt='cloud background'
				initial={{ x: 25, y: 50 }}
				animate={{ x: 0, y: 0 }}
				transition={{ duration: 1.5 }}
			></motion.img>
		</div>
	);
};

export default Login;
