import { MdPhotoCamera } from 'react-icons/md';
import { Avatar } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import './Avatar.css';
import { getToken, getUser } from '../../providers/authentication.js';
import { useEffect, useState } from 'react';
import { getUserData, uploadAvatar } from '../../providers/requests.js';

const AvatarComponent = ({ avatarUser, size }) => {
	const [profilePic, setProfilePic] = useState(null);
	const [user, setUser] = useState(null);

	const { id } = getUser();
	const token = getToken();

	const useStyles = makeStyles((theme) => ({
		root: {
			display: 'flex',
			'& > *': {
				margin: theme.spacing(1),
			},
		},
		small: {
			width: theme.spacing(5),
			height: theme.spacing(5),
		},
		large: {
			width: '150px',
			height: '150px',
		},
	}));

	const classes = useStyles();

	const updateAvatar = (name) => {
		name ? setProfilePic(`http://localhost:${process.env.REACT_APP_BEPORT}/images/${name}`) : setProfilePic(null);
	};

	const uploadHandler = async (image) => {
		const data = new FormData();
		data.append('image', image);

		const avatar = await uploadAvatar(user.id, token, data);

		updateAvatar(avatar);
	};

	useEffect(() => {
		(async () => {
			const userData = await getUserData(avatarUser?.id || id, token);
			setUser(userData);
			updateAvatar(userData.avatar);
		})();
	}, [avatarUser]);

	return (
		<div className={`container${user?.id === id ? '-user' : ''}`}>
			<Avatar alt='avatar' src={profilePic} className={`${classes[size]} avatar`} />

			{user?.id === id ? (
				<>
					<input type='file' className='upload-input' onChange={(e) => uploadHandler(e.target.files[0])}></input>
					<MdPhotoCamera className='upload-icon' />
				</>
			) : null}
		</div>
	);
};

export default AvatarComponent;
