import { motion } from 'framer-motion';
import { useHistory } from 'react-router-dom';
import './UserHome.css';

const UserHome = ({ darkTheme }) => {
	const history = useHistory();

	const containerVariants = {
		hidden: {
			opacity: 0,
		},
		visible: {
			opacity: 1,
		},
	};

	const startBtnVariants = {
		hover: {
			scale: 1.05,
			textShadow: '0px 0px 4px #e1f3f4',
			transition: {
				duration: 0.4,
				repeat: Infinity,
				repeatType: 'reverse',
			},
		},
		hidden: {
			opacity: 0,
		},
		visible: {
			opacity: 1,
			transition: { delay: 1, duration: 1.2 },
		},
	};

	return (
		<motion.div variants={containerVariants} initial='hidden' animate='visible' exit='exit' className='generator-container'>
			<motion.h2
				transition={{ duration: 2, type: 'spring', stiffness: 120 }}
				initial={{ y: '-100vh', opacity: 0 }}
				animate={{ opacity: 1, y: 0, x: 0, color: '#fff' }}
				style={{ textAlign: 'center', fontSize: '4rem', margin: '1rem 0' }}
			>
				Ready to start your trip ?
			</motion.h2>
			<motion.h2
				style={{ fontSize: '1.2rem', border: '2px white solid', borderRadius: '50px', padding: '10px 20px' }}
				variants={startBtnVariants}
				whileHover='hover'
				initial='hidden'
				animate='visible'
				className='start-button'
				onClick={() => history.push('/generate/routes')}
			>
				GET STARTED
			</motion.h2>
		</motion.div>
	);
};

export default UserHome;
