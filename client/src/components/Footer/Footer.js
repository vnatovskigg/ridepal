import './Footer.css';
import { FaTwitter, FaInstagram, FaFacebookSquare, FaApple, FaGooglePlay } from 'react-icons/fa';

const Footer = ({ darkTheme }) => {
	const date = new Date().getFullYear();
	return (
		<>
			<div className='footer'>
				<div className='social-media'>
					<a href='https://www.instagram.com' rel='noopener noreferrer' target='_blank'>
						<FaInstagram className={darkTheme ? 'social-media-icon dark-icon' : 'social-media-icon light-icon'} />
					</a>
					<a href='https://www.twitter.com' rel='noopener noreferrer' target='_blank'>
						<FaTwitter className={darkTheme ? 'social-media-icon dark-icon' : 'social-media-icon light-icon'} />
					</a>
					<a href='https://www.facebook.com' rel='noopener noreferrer' target='_blank'>
						<FaFacebookSquare className={darkTheme ? 'social-media-icon dark-icon' : 'social-media-icon light-icon'} />
					</a>
					<a href='https://www.apple.com/app-store/' rel='noopener noreferrer' target='_blank'>
						<FaApple className={darkTheme ? 'social-media-icon dark-icon' : 'social-media-icon light-icon'} />
					</a>
					<a href='https://play.google.com/store/apps/' rel='noopener noreferrer' target='_blank'>
						<FaGooglePlay className={darkTheme ? 'social-media-icon dark-icon' : 'social-media-icon light-icon'} />
					</a>
				</div>
				<div className='footer-copyright'>Copyright © {date} RidePal</div>
			</div>
		</>
	);
};

export default Footer;
