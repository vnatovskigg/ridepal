import { useEffect, useState } from 'react';
import { getAllPlaylists } from '../../providers/requests.js';
import { getToken } from '../../providers/authentication.js';
import PlaylistCard from '../PlaylistCard/PlaylistCard';
import Footer from '../Footer/Footer';
import './Explore.css';
import { Checkbox, FormControl, InputLabel, NativeSelect, Select } from '@material-ui/core';

const Explore = ({ darkTheme }) => {
	const [playlists, setPlaylists] = useState([]);
	const [genres, setGenres] = useState({
		Rock: false,
		Jazz: false,
		Blues: false,
		'Rap/Hip Hop': false,
		Electro: false,
	});
	const [selectedGenres, setSelectedGenres] = useState([]);
	const [sort, setSort] = useState('');

	const token = getToken();
	const toggle = ({ name, value }) => {
		setGenres((prev) => {
			const newVal = !prev[name];
			if (newVal) setSelectedGenres((prev) => new Set([...selectedGenres, value]));
			else setSelectedGenres((prev) => new Set([...prev].filter((g) => g !== value)));

			switch (name) {
				case 'Rock':
					return { ...prev, Rock: newVal };
				case 'Jazz':
					return { ...prev, Jazz: newVal };
				case 'Blues':
					return { ...prev, Blues: newVal };
				case 'Rap/Hip Hop':
					return { ...prev, 'Rap/Hip Hop': newVal };
				case 'Electro':
					return { ...prev, Electro: newVal };
				default:
					break;
			}
		});
	};

	const sortList = (value) => {
		setSort(value);
		setPlaylists((prev) => {
			return [...prev].sort((a, b) => (value === 'asc' ? a.name.localeCompare(b.name) : b.name.localeCompare(a.name)));
		});
	};

	useEffect(() => {
		(async () => {
			const playlists = await getAllPlaylists(token, selectedGenres);
			setPlaylists(playlists);
		})();
	}, [selectedGenres, token]);

	return (
		<>
			<div className='explore-wrapper'>
				<div className='explore-controls'>
					<div className='explore-controls-item'>
						<Checkbox id='rock' name='Rock' checked={genres['Rock']} value={152} onChange={(e) => toggle(e.target)} color='primary' />
						<label htmlFor='rock'>Rock</label>
					</div>
					<div className='explore-controls-item'>
						<Checkbox id='jazz' checked={genres['Jazz']} name='Jazz' value={129} onChange={(e) => toggle(e.target)} color='primary' />
						<label htmlFor='jazz'>Jazz</label>
					</div>
					<div className='explore-controls-item'>
						<Checkbox id='blues' checked={genres['Blues']} name='Blues' value={153} onChange={(e) => toggle(e.target)} color='primary' />
						<label htmlFor='blues'>Blues</label>
					</div>
					<div className='explore-controls-item'>
						<Checkbox id='rap' checked={genres['Rap/Hip Hop']} name='Rap/Hip Hop' value={116} onChange={(e) => toggle(e.target)} color='primary' />
						<label htmlFor='rap'>Rap/Hip Hop</label>
					</div>
					<div className='explore-controls-item'>
						<Checkbox id='electro' checked={genres['Electro']} onChange={(e) => toggle(e.target)} name='Electro' color='primary' value={106} />
						<label htmlFor='electro'>Electro</label>
					</div>
					<FormControl style={{ paddingLeft: '10px' }}>
						<NativeSelect value={sort} name='sort' onChange={(e) => sortList(e.target.value)} inputProps={{ 'aria-label': 'sort' }}>
							<option value='' disabled>
								Sort
							</option>
							<option value='asc'>A-z</option>
							<option value='desc'>Z-a</option>
						</NativeSelect>
					</FormControl>
				</div>
				<div className='explore-playlists-container'>
					{playlists.length &&
						playlists.map((p) => {
							return <PlaylistCard {...p} key={p.id} darkTheme={darkTheme} />;
						})}
				</div>
			</div>
			<Footer darkTheme={darkTheme} />
		</>
	);
};

export default Explore;
