import './Suggestions.css';

const Suggestions = ({ suggestions, setLocation, resetSuggestions }) => {
	return suggestions.length ? (
		<div className='suggestions-container'>
			<ul>
				{suggestions.map((item, i) => {
					const { adminDistrict, countryRegion, addressLine, locality, countryRegionIso2 } = item.address;
					return item.__type === 'Address' ? (
						<li
							key={i}
							onClick={(e) => {
								setLocation(e.target.innerText);
								resetSuggestions([]);
							}}
						>
							{`${addressLine}, ${locality || adminDistrict}, ${countryRegion || countryRegionIso2}`}
						</li>
					) : (
						<li
							key={i}
							onClick={(e) => {
								setLocation(e.target.innerText);
								resetSuggestions([]);
							}}
						>
							{`${item.name || item.address.locality || item.address.adminDistrict}, ${item.address.countryRegion}`}
						</li>
					);
				})}
			</ul>
		</div>
	) : null;
};

export default Suggestions;

// {`${item.address.streetName}${` ${item.address.houseNumber}` || ''}, ${item.address.locality},${` ${item.address.postalCode}` || ''}${
// 	item.address.countryRegion
// }`}
