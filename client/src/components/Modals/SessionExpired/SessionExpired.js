import { Button, Dialog, DialogTitle } from '@material-ui/core';
import React from 'react';
import { Link } from 'react-router-dom';
import './SessionExpired.css';

const SessionExpired = ({ open, handleClose }) => {
	return (
		<Dialog open={open} onClose={handleClose}>
			<div className='session-expired'>
				<div className='alert-header'>
					<DialogTitle>
						<h1>Session expired</h1>
					</DialogTitle>
					<hr />
				</div>
				<div className='alert-body'>
					<div className='alert-message'>
						<h3>Your session has expired. Please log in again.</h3>
					</div>
					<div className='alert-actions'>
						<Link to='/login' onClick={handleClose}>
							<Button color='primary' variant='contained'>
								GO TO LOGIN PAGE
							</Button>
						</Link>
					</div>
				</div>
			</div>
		</Dialog>
	);
};

export default SessionExpired;
