import { useEffect, useState } from 'react';
import { Link, useHistory, useParams, withRouter } from 'react-router-dom';
import { getToken, getUser } from '../../providers/authentication.js';
import { createPlaylist, deletePlaylist, getUserData, getUserPlaylists, updatePlaylist } from '../../providers/requests.js';
import { AiOutlineCheck, AiFillPlusCircle, AiFillMinusCircle } from 'react-icons/ai';
import { FaRegEdit } from 'react-icons/fa';
import convertPlaytime from '../../helpers/timeConverter.js';
import './Playlist.css';
import Track from '../Track/Track.js';
import Footer from '../Footer/Footer.js';
import Button from '../Button/Button.js';
import generateDescription from '../../helpers/generateDescription.js';

const Playlist = ({ location, darkTheme }) => {
	const [playlist, setPlaylist] = useState(location.state?.playlist || null);
	const [poster, setPoster] = useState(location.state?.imageUrl || null);
	const [playlistName, setName] = useState(location.state?.playlist?.name || '');
	const [genres, setGenres] = useState(location.state?.genres || null);
	const [playlistUser, setPlaylistUser] = useState(null);
	const [description, setDescription] = useState(generateDescription(location.state?.playlist) || '');
	const [updateMsg, setUpdateMsg] = useState('');

	const { id, pid } = useParams();
	const user = getUser();
	const token = getToken();
	const history = useHistory();
	const playtime = playlist && convertPlaytime(playlist.totalPlaytime);
	const sameUser = user && playlistUser && user.id === playlistUser.id;
	useEffect(() => {
		(async () => {
			if (id && pid) {
				const playlistData = (await getUserPlaylists(id, token, pid))[0];
				const user = await getUserData(id, token);

				setDescription(generateDescription(playlistData));
				setPlaylist(playlistData);
				setPoster(playlistData.poster);
				setName(playlistData.name);
				setPlaylistUser(user);
			} else {
				setPlaylistUser(user);
			}
		})();
	}, []);

	const updateHandler = async () => {
		if (id && pid) {
			if (playlistName === playlist.name) setUpdateMsg('Pick a new name to update');
			else {
				const updated = await updatePlaylist(playlistName, id, pid, token);
				if (updated?.message?.includes('Success')) setUpdateMsg('Updated successfully');
				else setUpdateMsg('Try again');
			}
			setTimeout(() => {
				setUpdateMsg('');
			}, 2000);
		}
	};

	const addPlaylistHandler = async () => {
		if (!playlistName) setUpdateMsg('First choose a playlist name');
		else {
			const created = await createPlaylist(playlistName, playlistUser.id, playlist.totalPlaytime, playlist.trackList, genres, poster, token);
			if (created?.message.includes('Success')) {
				setUpdateMsg('Playlist added successfuly. Enjoy your trip!');

				setTimeout(() => {
					history.push(`/profile/${playlistUser.id}`);
				}, 2000);
			} else setUpdateMsg('Try again');
		}
	};

	const deletePlaylistHandler = async () => {
		const deleted = await deletePlaylist(user.id, playlist.id, token);

		if (deleted?.message?.includes('Success')) {
			history.push(`/profile/${user.id}`);
		} else {
			setUpdateMsg('Try again');
		}
	};

	return (
		<>
			<div className='playlist-wrapper'>
				<div className='playlist-container'>
					{playlist && user ? (
						<>
							<div className='playlist-header' style={{ backgroundImage: `url(${poster})` }}>
								<div className='playlist-header-top'>
									<h2>Playlist name</h2>
									<div className='playlist-name-actions'>
										{sameUser && (
											<>
												{id && pid ? (
													sameUser ? (
														<Button text='DELETE' handleClick={deletePlaylistHandler} />
													) : null
												) : (
													<Button text='CREATE' handleClick={addPlaylistHandler} />
												)}
											</>
										)}
									</div>
								</div>
								{updateMsg ? <span className='update-msg'>{updateMsg}</span> : null}
								<div className='playlist-name-container'>
									<input
										type='text'
										id='playlist-name'
										size={playlistName.length ? playlistName.length + 2 : 13}
										className={sameUser ? 'playlist-name playlist-name-self' : 'playlist-name'}
										placeholder='playlist name'
										value={playlistName}
										onChange={(e) => setName(e.target.value)}
									/>
									{sameUser && (
										<div className='playlist-name-actions'>
											{id && pid && <AiOutlineCheck className='change-name-control' onClick={updateHandler} />}
											<label htmlFor='playlist-name' className='change-name-control'>
												<FaRegEdit />
											</label>
										</div>
									)}
								</div>
								<h6 className='playlist-subtitle'>{description}</h6>
								<h5>
									by{' '}
									{playlistUser && (
										<Link to={`/profile/${playlistUser.id}`} className={darkTheme ? 'username-dark' : 'username'}>
											{playlistUser.username}
										</Link>
									)}
									| {playlist.trackList.length} songs | {playtime}
								</h5>
							</div>
							<div className={darkTheme ? 'dark-tracks' : 'light-tracks'}>
								{playlist.trackList.map((track, i) => {
									return <Track id={track.trackDeezerId} key={i} />;
								})}
							</div>
							<div className='playlist-form-actions'>
								{!id && !pid && (
									<>
										<Button
											text='Back'
											handleClick={() =>
												history.push({
													pathname: '/generate/genres',
													state: {
														startLocation: location.state?.startLocation,
														endLocation: location.state?.endLocation,
														genres: location.state?.genres,
														duration: location.state?.duration,
													},
												})
											}
										/>
										<Button text='My profile' handleClick={() => history.push(`/profile/${user.id}`)} />
									</>
								)}
							</div>
						</>
					) : (
						<h3>Loading...</h3>
					)}
				</div>
			</div>
			<Footer darkTheme={darkTheme} />
		</>
	);
};

export default withRouter(Playlist);

// #2A626E
