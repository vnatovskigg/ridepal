import { Route, Redirect } from 'react-router-dom';

const GuardedRoute = ({ component: Component, isLoggedIn, darkTheme, ...rest }) => {
	return <Route {...rest} render={(props) => (isLoggedIn ? <Component darkTheme={darkTheme} {...props} /> : <Redirect to={'/'} />)} />;
};

export default GuardedRoute;
