import './PlaylistCard.css';
import convertPlaytime from '../../helpers/timeConverter.js';
import { Link, useHistory } from 'react-router-dom';
import { useEffect, useState } from 'react';
import { getUserData } from '../../providers/requests.js';
import generateDescription from '../../helpers/generateDescription.js';
import { getToken } from '../../providers/authentication';

const PlaylistCard = (props) => {
	const [poster, setPoster] = useState(props.poster || null);
	const [playlistUser, setPlaylistUser] = useState(null);

	const history = useHistory();
	const token = getToken();
	const playtime = convertPlaytime(props.totalPlaytime);
	const descriptionString = generateDescription(props);

	useEffect(() => {
		(async () => {
			const user = await getUserData(props.userId, token);
			setPlaylistUser(user);
		})();
	}, []);

	return (
		<div className={`playlistCard-container${props.darkTheme ? '-dark' : ''}`}>
			<img
				src={poster}
				className='playlist-poster'
				alt={props.name}
				onClick={() => history.push({ pathname: `/${props.userId}/playlists/${props.id}`, state: { imageUrl: `${poster}` } })}
			/>
			<div className='playlist-info'>
				<h3>{props.name}</h3>
				<p id="username-paragraph">
					by{' '}
					{playlistUser && (
						<Link to={`/profile/${playlistUser.id}`} className={props.darkTheme ? 'username-dark' : 'username'}>
							{playlistUser.username}
						</Link>
					)}
				</p>
				<p>{playtime}</p>
				<p className='playlist-card-desc'>{descriptionString}</p>
				<p className='playlist-tags'>
					Tags:{' '}
					{props.genres.map((genre) => (
						<span className={`${props.darkTheme ? 'tag-dark' : 'tag'}`}>{genre.name}</span>
					))}
				</p>
			</div>
		</div>
	);
};

export default PlaylistCard;
