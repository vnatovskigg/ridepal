import { Avatar, Container, Grid } from '@material-ui/core';
import { useEffect, useRef, useState } from 'react';
import { useParams } from 'react-router-dom';
import { getToken } from '../../providers/authentication.js';
import { getUserData, getUserPlaylists } from '../../providers/requests.js';
import PlaylistCard from '../PlaylistCard/PlaylistCard.js';
import Footer from '../Footer/Footer';
import './UserProfile.css';
import ErrorNotFound from '../Errors/Error404/Error404.js';
import AvatarComponent from '../Avatar/Avatar.js';

const UserProfile = ({ darkTheme }) => {
	const [user, setUser] = useState({});
	const [playlists, setPlaylists] = useState([]);
	const { id } = useParams();
	const [profilePic, setProfilePic] = useState(null);
	const token = getToken();
	const isMounted = useRef(false);

	useEffect(() => {
		(async () => {
			isMounted.current = true;
			if (isMounted) {
				// get user details
				const user = await getUserData(id, token);
				setUser(user);
				// get playlists
				const userPlaylists = await getUserPlaylists(id, token);
				setPlaylists(userPlaylists);
				user.avatar ? setProfilePic(`http://localhost:${process.env.REACT_APP_BEPORT}/images/${user.avatar}`) : setProfilePic(null);

				return () => (isMounted.current = false);
			}
		})();
	}, [id, token]);

	return user.message ? (
		<ErrorNotFound />
	) : (
		<>
			<div className='user-profile'>
				<Container maxWidth='lg'>
					<Grid container direction='column' justify='center' alignItems='center' spacing={3}>
						<h1>{`${user.username}'s Profile`}</h1>
						<div className='avatar-mobile'>
							<Avatar alt='avatar' src={profilePic} className={`my-profile-avatar`} />
						</div>
						<div className='avatar-desktop'>
							<AvatarComponent avatarUser={profilePic} size='large' />
						</div>

						<h3>Playlists:</h3>
						<div className='playlists'>
							{playlists.length ? (
								playlists.map((playlist) => {
									return <PlaylistCard {...playlist} darkTheme={darkTheme} />;
								})
							) : (
								<h4>No playlists yet</h4>
							)}
						</div>
					</Grid>
				</Container>
			</div>
			<Footer darkTheme={darkTheme} />
		</>
	);
};

export default UserProfile;
