import { motion } from 'framer-motion';
import { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { getTripDuration } from '../../providers/requests.js';
import Footer from '../Footer/Footer';
import RoutesForm from '../RoutesForm/RoutesForm';
import Loader from '../Loader/Loader';
import './Routes.css';
import Button from '../Button/Button.js';

const Routes = ({ darkTheme, location }) => {
	const [startLocation, setStartLocation] = useState(location.state?.startLocation || '');
	const [endLocation, setEndLocation] = useState(location.state?.endLocation || '');
	const [loading, setLoading] = useState(false);
	const [errors, setErrors] = useState({
		start: false,
		end: false,
	});
	const history = useHistory();

	const containerVariants = {
		hidden: {
			opacity: 0,
			x: '100vw',
		},
		visible: {
			opacity: 1,
			x: 0,
			transition: {
				type: 'spring',
				delay: 0.5,
			},
		},
		exit: {
			x: '-100vw',
			transition: { ease: 'easeInOut' },
		},
	};

	const handleClick = async () => {
		if (startLocation && endLocation) {
			const tripDuration = await getTripDuration(startLocation, endLocation);

			if (!tripDuration) setErrors({ ...errors, noRoute: true });
			else {
				setLoading(true);

				setTimeout(() => {
					setLoading(false);
					history.push({
						pathname: '/generate/genres',
						state: {
							duration: tripDuration,
							startLocation: startLocation,
							endLocation: endLocation,
						},
					});
				}, Math.floor(Math.random() * 1000 * 5));
			}
		} else {
			setErrors({
				start: startLocation === '',
				end: endLocation === '',
			});
		}
	};

	return (
		<>
			<div className='home-wrapper'>
				<motion.div variants={containerVariants} initial='hidden' animate='visible' exit='exit' className='routes-form-container'>
					<div className='routes-form-root'>
						{errors.noRoute && <p className='route-error'>Can't find route. Try different locations.</p>}
						<RoutesForm
							startLocation={startLocation}
							endLocation={endLocation}
							setStartLocation={setStartLocation}
							setEndLocation={setEndLocation}
							errors={errors}
							setErrors={setErrors}
							darkTheme={darkTheme}
						/>
						<div className='routes-form-actions'>
							<Button text='BACK' handleClick={() => history.push('/')} />
							<Button text='NEXT' handleClick={handleClick} />
						</div>
					</div>
				</motion.div>
			</div>
			{loading && <Loader darkTheme={darkTheme} />}
			<Footer darkTheme={darkTheme} />
		</>
	);
};

export default Routes;
