import { motion } from 'framer-motion';
import { Link } from 'react-router-dom';
import './GuestHome.css';
import FirstHomeImageLight from '../../assets/homepage_photo_4.png';
import FirstHomeImageDark from '../../assets/homepage_photo_5.png';

const GuestHome = ({ darkTheme }) => {
	const registerBtnVariants = {
		hover: {
			scale: 1.1,
			boxShadow: '0px 0px 6px #e1f3f4',
			transition: {
				duration: 0.4,
				repeat: Infinity,
				repeatType: 'reverse',
			},
		},
		hidden: {
			opacity: 0,
		},
		visible: {
			opacity: 1,
			transition: { delay: 1, duration: 1.2 },
		},
	};

	return (
		<div className='home-wrapper'>
			<div className='home-container'>
				<div className='home-container-row'>
					<div className='home-container-column'>
						<motion.h2
							transition={{ duration: 3, type: 'spring', stiffness: 120 }}
							initial={{ x: '-100vh', opacity: 0.75 }}
							animate={{ opacity: 1, x: 0, color: '#fff' }}
							style={{ textAlign: 'center', fontSize: '4rem', margin: '1rem 0' }}
						>
							Tired of searching for music?
						</motion.h2>
						<motion.div
							initial={{ opacity: 0 }}
							transition={{ delay: 1, duration: 1.2 }}
							animate={{ opacity: 1, color: '#fff' }}
							style={{ display: 'flex', flexDirection: 'column' }}
						>
							<h3 style={{ fontSize: '1.5rem' }}>We got you! Create a playlist by your taste!</h3>
							<Link to='/register'>
								<motion.h2
									variants={registerBtnVariants}
									whileHover='hover'
									style={{ fontSize: '1.2rem', border: '2px white solid', borderRadius: '50px', padding: '10px 20px', display: 'inline-flex' }}
								>
									REGISTER NOW
								</motion.h2>
							</Link>
						</motion.div>
					</div>
					<motion.div
						className='home-container-column home-img'
						initial={{ opacity: 0 }}
						transition={{ delay: 1, duration: 1.2 }}
						animate={{ opacity: 1 }}
					>
						<img
							src={darkTheme ? FirstHomeImageDark : FirstHomeImageLight}
							alt='listen music daily with ridepal'
							className='home-container-column-img'
						/>
					</motion.div>
				</div>
			</div>
		</div>
	);
};

export default GuestHome;
