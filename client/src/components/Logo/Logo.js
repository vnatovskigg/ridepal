import { Link } from 'react-router-dom';
import RidePalLogoDark from '../../assets/ridepal_logo_400px.png';
import RidePalLogoLight from '../../assets/ridepal_white_logo_400px.png';
import './Logo.css';

const Logo = ({ darkTheme }) => {
	return (
		<Link to='/'>
			<div className='logo'>
				<img src={darkTheme ? RidePalLogoDark : RidePalLogoLight} width='100px' alt='RidePal Logo' />
				<div className='logo-slogan'>
					<h1>RidePal</h1>
					<p>YOUR RIDE WITH MUSIC</p>
				</div>
			</div>
		</Link>
	);
};

export default Logo;
