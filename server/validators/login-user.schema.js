export default {
	email: (value) => typeof value === 'string' && value.length > 5 && value.length <= 100,
	password: (value) => typeof value === 'string' && value.length >= 8 && value.length <= 128,
};
