import validator from 'validator';

export default {
	username: (value) => typeof value === 'string' && value.length >= 3 && value.length <= 100 && value.split(' ').length === 1,
	password: (value) => typeof value === 'string' && value.length >= 8 && value.length <= 128,
	email: (value) => typeof value === 'string' && value.length >= 5 && value.length <= 100 && validator.isEmail(value),
	role: (value) => (typeof value === 'string' && value === 'Admin') || value === 'User',
	isDeleted: (value) => typeof value === 'number' && value >= 0 && value <= 1,
	avatar: (value) => typeof value === 'string' && value.length > 0 && value.length <= 255,
};
