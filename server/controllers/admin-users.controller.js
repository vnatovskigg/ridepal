import express from 'express';
import bcrypt from 'bcrypt';
import transformBody from '../middlewares/transform-body.js';
import { getUsers, getUserBy, updateUser, deleteUser, getBanStatus, unBan, banUser, getUsersByUsername } from '../services/admin.service.js';
import userValidator from '../validators/update-user.schema.js';
import validateUpdateBody from '../middlewares/validate-update-body.js';

const adminUsersController = express.Router();

adminUsersController

	// get all users
	.get('/', async (req, res) => {
		const { username } = req.query;
		let users;

		if (username) users = await getUsersByUsername(username);
		else users = await getUsers();

		res.status(200).send(users);
	})

	// update user by user id
	.put('/:id', transformBody(userValidator), validateUpdateBody('users', userValidator), async (req, res) => {
		const id = +req.params.id;
		const { password } = req.body;
		const roleId = req.body.role === 'Admin' ? 1 : 2;

		if (req.body.role) delete req.body.role;

		const found = await getUserBy('id', id);

		if (!found) return res.status(404).send({ message: 'User not found.' });
		else {
			if (password) {
				const passHash = await bcrypt.hash(password, 10);
				req.body.password = passHash;
			}
			const updated = await updateUser(id, { ...req.body, roleId });
			if (updated.error) return res.status(404).send({ message: 'Something went wrong... Please try again later.' });
			else return res.status(201).send(updated);
		}
	})

	// delete user by id
	.delete('/:id', async (req, res) => {
		const id = +req.params.id;

		const found = await getUserBy('id', id);

		if (!found) return res.status(404).send({ message: 'User not found.' });
		else {
			const deleted = await deleteUser(id);
			if (!deleted) return res.status(404).send({ message: 'Something went wrong... Please try again later.' });
			else return res.status(200).send({ message: `Success: user with id ${id} was deleted!` });
		}
	})

	// ban/unBan user
	.put('/banstatus/:id', async (req, res) => {
		const id = +req.params.id;

		const found = await getUserBy('id', id);

		if (!found) return res.status(404).send({ message: 'User not found.' });
		else {
			const banStatus = await getBanStatus(id);

			if (!banStatus) {
				let expirationDate = new Date();
				expirationDate.setDate(expirationDate.getDate() + 7);

				const banned = await banUser(id, expirationDate);

				if (banned) return res.status(200).send({ message: `Success: user ${id} was banned until ${expirationDate}` });
				else return res.status(404).send({ message: 'Something went wrong... Please try again later.' });
			} else {
				const unbanned = await unBan(id);

				if (unbanned) return res.status(200).send({ message: `Success: user ${id} was unbanned` });
				else return res.status(404).send({ message: 'Something went wrong... Please try again later.' });
			}
		}
	});

export default adminUsersController;
