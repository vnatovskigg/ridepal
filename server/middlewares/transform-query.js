export default (req, _, next) => {
	const acceptedQueries = ['title', 'genre', 'artist', 'album', 'order', 'page', 'pageSize'];
	Object.keys(req.query).map((q) => (!acceptedQueries.includes(q) ? delete req.query[q] : null));
	next();
};
