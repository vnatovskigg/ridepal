import db from './pool.js';
import jwt from 'jsonwebtoken';
import dotenv from 'dotenv';

/**
 * Checks if given token is in the blacklist
 * @param {string} token
 * @returns {boolean}
 */
export const checkTokenBlacklist = async (token) => {
	const SQL = `
		SELECT * FROM tokens_blacklist
		WHERE token = ?
	`;

	try {
		const blacklisted = await db.query(SQL, [token]);
		if (blacklisted[0]) return true;
		else return false;
	} catch (error) {
		return { error: error.message };
	}
};

/**
 * Checks if given JWT Bearer token is invalid
 * @param {string} token
 * @returns object with key error if the token is invalid
 */
export const verifyUserToken = (token) => {
	const config = dotenv.config().parsed;

	return jwt.verify(token, config.SECRET_KEY, (err, user) => {
		if (err) return { error: 'Invalid bearer token.' };
		else return {};
	});
};

/**
 * Puts token inside the blacklist
 * @param {string} token
 */
export const logoutUser = async (token) => {
	const SQL = `
		INSERT INTO tokens_blacklist (token)
		VALUES (?)
	`;

	try {
		await db.query(SQL, [token]);
	} catch (error) {
		return { error: error.message };
	}
};
