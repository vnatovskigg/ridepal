import db from './pool.js';

/**
 * Get all tracks with optional order, page or pageSize.
 * @param {string} order 'asc' or 'desc'
 * @param {number} page default is 0
 * @param {number} pageSize default is 20 and max is 100
 * @returns an array of track objects.
 */
export const getAllTracks = async ({ order = 'asc', page = 0, pageSize = 20 }) => {
	if (page < 0) page = 0;
	if (pageSize < 0) pageSize = 20;
	if (pageSize > 5000) pageSize = 5000;

	const orderSQL = order && (order === 'asc' || order === 'desc') ? `ORDER BY t.title ${order.toUpperCase()}` : '';

	const SQL = `
    SELECT t.id, t.deezerId AS trackDeezerId, t.title, t.duration, t.rank, t.previewUrl, g.deezerId AS genreDeezerId, g.name AS genreTitle, g.pictureMedium AS genrePictureMedium, a.deezerId AS albumDeezerId, a.title AS albumName, a.coverMedium AS albumCoverMedium, artists.deezerId AS artistDeezerId, artists.name AS artistName, artists.pictureMedium AS artistPictureMedium 
    FROM tracks t 
    JOIN genres g ON g.deezerId = t.genreId 
    JOIN albums a ON a.deezerId = t.albumId 
    JOIN artists ON artists.deezerId = a.artistId 
    WHERE t.isDeleted = 0 
    ${orderSQL}
    LIMIT ${page * pageSize}, ${pageSize}
	`;

	try {
		return await db.query(SQL);
	} catch (error) {
		return { error: error.message };
	}
};

/**
 * Get all tracks with optional order, page or pageSize and apply filter
 * with the queries title, genreName, artistName or albumName.
 * @param {string} order 'asc' or 'desc'
 * @param {number} page default is 0
 * @param {number} pageSize default is 20 and max is 100
 * @returns an array of track objects.
 */
export const filterTracksBy = async ({ order = 'asc', page = 0, pageSize = 5, ...queries }) => {
	if (page < 0) page = 0;
	if (pageSize < 0) pageSize = 20;
	if (pageSize > 2000) pageSize = 2000;

	const orderSQL = order && (order === 'asc' || order === 'desc') ? `ORDER BY t.title ${order.toUpperCase()}` : '';

	const SQL = `
    SELECT t.id, t.deezerId AS trackDeezerId, t.title, t.duration, t.rank, t.previewUrl, g.deezerId AS genreDeezerId, g.name AS genreName, g.pictureMedium AS genrePictureMedium, a.deezerId AS albumDeezerId, a.title AS albumName, a.coverMedium AS albumCoverMedium, artists.deezerId AS artistDeezerId, artists.name AS artistName, artists.pictureMedium AS artistPictureMedium 
    FROM tracks t 
    JOIN genres g ON g.deezerId = t.genreId 
    JOIN albums a ON a.deezerId = t.albumId 
    JOIN artists ON artists.deezerId = a.artistId 
    WHERE t.isDeleted = 0 
    AND ${Object.entries(queries)
			.map(([q, val]) => `${q} LIKE "%${val}%"`) // title LIKE "%Test%"
			.join('\nAND ')}
    ${orderSQL}
    LIMIT ${page * pageSize}, ${pageSize}
	`;

	try {
		const res = await db.query(SQL);
		return res;
	} catch (error) {
		return { error: error.message };
	}
};

/**
 * Search for a track by field name. Fields can be id, trackDeezerId, title, duration,
 * rank, previewUrl, genreDeezerId, genreTitle, genrePictureMedium, albumDeezerId,
 * albumTitle, albumCoverMedium, artistDeezerId, artistName, artistPictureMedium.
 * @param {string} field
 * @param {any} value
 * @returns the found track object.
 */
export const getTrackBy = async (field, value) => {
	const SQL = `
    SELECT t.id, t.deezerId AS trackDeezerId, t.title, t.duration, t.rank, t.previewUrl, g.deezerId AS genreDeezerId, g.name AS genreTitle, g.pictureMedium AS genrePictureMedium, a.deezerId AS albumDeezerId, a.title AS albumTitle, a.coverMedium AS albumCoverMedium, artists.deezerId AS artistDeezerId, artists.name AS artistName, artists.pictureMedium AS artistPictureMedium 
    FROM tracks t 
    JOIN genres g ON g.deezerId = t.genreId 
    JOIN albums a ON a.deezerId = t.albumId 
    JOIN artists ON artists.deezerId = a.artistId 
    WHERE t.isDeleted = 0 
    AND t.${field} = ?
	`;

	try {
		const foundTrack = await db.query(SQL, [value]);
		return foundTrack[0];
	} catch (error) {
		return { error: error.message };
	}
};

/**
 * Updates track with object containing key-value pairs.. Updated data can have
 * the following keys: id, deezerId, title, duration, rank, previewUrl, albumId, genreId.
 * @param {number} deezerId
 * @param {object} updateData
 * @returns the updated track object
 */
export const updateTrack = async (deezerId, updateData) => {
	const data = Object.entries(updateData);
	const SQL = `
			UPDATE tracks
			SET
				${data.map(([key, val]) => `${key} = "${val}"`)}
			WHERE deezerId = ?;
      AND isDeleted = 0
    `;

	const SQL2 = `
    SELECT t.id, t.deezerId AS trackDeezerId, t.title, t.duration, t.rank, t.previewUrl, g.deezerId AS genreDeezerId, g.name AS genreTitle, g.pictureMedium AS genrePictureMedium, a.deezerId AS albumDeezerId, a.title AS albumTitle, a.coverMedium AS albumCoverMedium, artists.deezerId AS artistDeezerId, artists.name AS artistName, artists.pictureMedium AS artistPictureMedium 
    FROM tracks t 
    JOIN genres g ON g.deezerId = t.genreId 
    JOIN albums a ON a.deezerId = t.albumId 
    JOIN artists ON artists.deezerId = a.artistId 
    WHERE t.isDeleted = 0 
    AND t.deezerId = ?
	`;

	try {
		await db.query(SQL, [deezerId]);
		const updatedTrack = await db.query(SQL2, [deezerId]);
		return updatedTrack[0];
	} catch (error) {
		return { error: error.message };
	}
};

/**
 * Creates new track with additional id provided by Deezer
 * @param {number} deezerId
 * @param {string} title
 * @param {number} duration
 * @param {number} rank
 * @param {string} previewUrl
 * @param {number} albumId
 * @param {number} genreId
 * @returns the created track object
 */
export const addDeezerTrack = async (deezerId, title, duration, rank, previewUrl, albumId, genreId) => {
	const SQL1 = `
    INSERT INTO tracks(deezerId, title, duration, rank, previewUrl, albumId, genreId)
    VALUES(?, ?, ?, ?, ?, ?, ?);
  `;

	const SQL2 = `
    SELECT t.id, t.deezerId AS trackDeezerId, t.title, t.duration, t.rank, t.previewUrl, g.deezerId AS genreDeezerId, g.name AS genreTitle, g.pictureMedium AS genrePictureMedium, a.deezerId AS albumDeezerId, a.title AS albumTitle, a.coverMedium AS albumCoverMedium, artists.deezerId AS artistDeezerId, artists.name AS artistName, artists.pictureMedium AS artistPictureMedium 
    FROM tracks t 
    JOIN genres g ON g.deezerId = t.genreId 
    JOIN albums a ON a.deezerId = t.albumId 
    JOIN artists ON artists.deezerId = a.artistId 
    WHERE t.isDeleted = 0 
    AND t.deezerId = ?
  `;

	try {
		await db.query(SQL1, [deezerId, title, duration, rank, previewUrl, albumId, genreId]);
		const newTrack = await db.query(SQL2, [deezerId]);
		return newTrack[0];
	} catch (error) {
		return { error: error.message };
	}
};


export const getGenres = async () => {
	const SQL = `
		SELECT * FROM genres
		WHERE isDeleted = 0
	`

	try {
		return await db.query(SQL);
	} catch (e) {
		console.log(e);
	}
}

export const getGenreById = async (id) => {
	const SQL = `
		SELECT * FROM genres
		WHERE isDeleted = 0
		AND deezerId = ?;
	`

	try {
		const found = await db.query(SQL, [id]);
		if (found) return found[0];
		else return null;
	} catch (err) {
		console.log(err);
		return null;
	}
}