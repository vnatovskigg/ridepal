import jwt from 'jsonwebtoken';
import dotenv from 'dotenv';

const config = dotenv.config().parsed;

const createToken = (payload) => {
	const token = jwt.sign(payload, config.SECRET_KEY, { expiresIn: '6h' });

	return token;
};

export default createToken;
