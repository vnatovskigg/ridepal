import passportJwt from 'passport-jwt';
import dotenv from 'dotenv';

const config = dotenv.config().parsed;

const options = {
	secretOrKey: config.SECRET_KEY,
	jwtFromRequest: passportJwt.ExtractJwt.fromAuthHeaderAsBearerToken(),
};

const jwtStrategy = new passportJwt.Strategy(options, async (payload, done) => {
	const userData = {
		id: payload.id,
		username: payload.username,
		role: payload.role,
	};

	done(null, userData);
});

export default jwtStrategy;
