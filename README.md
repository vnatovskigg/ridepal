# Welcome to RidePal
## hosted on https://ridepal-12.web.app/

### by [Velizar Natovski](https://github.com/vnatovskigg) and [Kaloyan Nikolov](https://gitlab.com/xellstar)

# Setup guide

Follow the below described process in order to get the application running on your local environment.

## Create the datase

RidePal is using MariaDB for database service. In order to set it up the locally, please follow the process:

1. Find the file **create_db_script.sql**, located in **_/server/database_** and **execute** it in your MySQL tool of choice (Workbench or other).

2. When your database is created, you can proceed with filling it with data. For that purpose, a **seed** script is made available. It's location is again - **_/server/database/seed.js_**.

3. In order to run the seed script - run the command **npm run seed** in your terminal while in the server folder.
   Note: the seed script doesn't stop by itself currently therefore you need to stop it after you see that the database is no longer getting new entries (~60 sec).

4. The provided seed script creates an admin user automatically, which can be used to login and browse the app. Login credentials:
   _username_: admin@example.com
   _password_: 123123123

5. All set. Your database should now be populated with data and you are ready to dive into the next step in setting up environmental variables.

## Create environmental variables in .env files

Both the **_/server_** and **_/client_** directories need to have **.env** file created and filled with the respective environmental variables in order for the application to function properly.

1. Server directory - feel free to follow the below template in creating your .env file in the server directory

![server-env](server-env.PNG)

```
PORT=5000
HOST=localhost
DBPORT=3306
USER=your_db_username
PASSWORD=your_db_password
DATABASE=ridepaldb
SECRET_KEY=super-s3cr3t-key
```


2. Client directory - the same has to be done in the **client directory** as it uses it's own environment variables to function.

You are welcome to use the pasted external API keys if you don't have ones:

![client-env](client-env.PNG)

```
REACT_APP_BEPORT=5000
REACT_APP_PIXABEY_API=21818606-1a6028b39ac8fcfeaf607a417
REACT_APP_BING_KEY=Ar2BuQaGfNiQ38nDSx5E-fuCifsnX3cj6F-DFTpdmolbTd8eUuUTyXvYPRHP2Une
```

3. You're all set !

### \***_A quick disclaimer before you continue_**

\*\*When creating new playlists, we're storing randomly generated cover pictures coming from Pixabey's API. Their image urls are valid for a limited time, hence why for some older playlists, the image might be missing.

\*\*Even though there are checks for duplication entries, when running the seed script sometimes you can get errors in the console. Those should be ignored as they doesn't affect the app currently.

## Install dependencies and starting the application

1. In the server directory run the command **npm install** to install all dependencies needed to run the app.

2. Do the same in the client directory.

3. Run the script **npm run start** in both server and client directories to fire up the server and client respectively (or **npm run start:dev** in **_/server_**).

4. That's it. Welcome to RidePal !!!

## API

### Public Endpoints

#### Authentication

- POST /v1/api/login

  - Login an existing user.
  - Example request and response:

        Request body:

        ```
          {
            "email": "user@example.com",
            "password": "12345678",
          }
        ```

        Response:

        ```

        {
          "message": "Login successful!",
          "token": "eyJhbGSiOiJIUSI1NiISSnR5cCI6IkpXVCJ9.eyJpZCI6MSwidXNlcm5hbHJUiOiJBZG1pbiIsInJvbGUiOiJBZG1pbiIsImhdCI6MTYyMzMwMHT4MSwiZXhwIjoxNjIzMzIyODgxfQ.XiS1uXVSkge38BmnHKZ53plJM4UrOJHakuMfuctxpsk"
        }

        ```

- POST /v1/api/register

  - Register new user.
  - Example request and response:

    Request body:

    ```
      {
        "email": "user@example.com",
        "password": "12345678",
        "username": "new_user"
      }
    ```

    Response:

    ```
    {
      "id": 1,
      "username": "new_user",
      "email": "user@example.com",
      "roleId": 1
    }
    ```

- POST /v1/api/logout

  - todo description
  - Example response:

    ```
    {
      "message": "Logout successful!"
    }
    ```

### Private Endpoints

#### Users only

- GET /v1/api/users/:id

  - Get information about single user by user id.
  - Example response:

    ```
    {
      "id": 1,
      "username": "Admin",
      "email": "admin@example.com",
      "createdOn": "2021-06-09T18:47:43.000Z",
      "role": "Admin",
      "avatar": "1623265060532.png"
    }
    ```

- GET /v1/api/users/:id/playlists

  - Get all playlists of a user by user id.
  - Example response:

    ```
    {
      [
        {
          "id": 1,
          "userId": 1,
          "name": "mon-sofia new title",
          "totalPlaytime": 7137,
          "rank": null,
          "createdOn": "2021-06-09T18:57:24.000Z",
          "isDeleted": 0,
          "poster": "https://pixabay.com/get/gb5637da46515b3696fa9bd449da986225b56a4d03813a79fd7f328735186e1708f67bccd2d6ad4db7f1dc0c302908fa156e80780a1e2c66e9c0d3084dd57891c_640.jpg",
          "trackList": [],
          "genres": []
        }
      ]
    ```

- PUT /v1/api/users/:id/playlists/:pid

  - Edit your own playlist
  - Example request and response:

    Request:

    ```
      todo
    ```

    Response:

    ```
      todo
    ```

- GET /v1/api/users/playlists/all

  - Get all playlists of all users.
  - Response will be an array of playlists (see GET al playlists of a user by user id)

- GET /v1/api/tracks

  - Get all tracks. Optional queries are:
    - title - track's title
    - artist - track's artist
    - album - track's album
    - genre - track's genre
    - order - results order - can be 'asc' or 'desc'.
    - page - pagination for the results. The page count starts from 0.
    - pageSize - size of results on a page. Default is 5. Max page size is 100.
  - Example request endpoint and response:

  Request:

  ```
    http://localhost:5000/v1/api/tracks?genre=rap/hip+hop&page=0&pageSize=3&order=asc
  ```

  Response:

  ```
    [
      {
      "id": 2230,
      "trackDeezerId": 375689841,
      "title": "(Intro) I'm so Grateful (feat. Sizzla)",
      "duration": 298,
      "rank": 305792,
      "previewUrl": "https://cdns-preview-c.dzcdn.net/stream/c-cc1b63606a0b467144cc0253768ee5c9-8.mp3",
      "genreDeezerId": 116,
      "genreName": "Rap/Hip Hop",
      "genrePictureMedium": "https://cdns-images.dzcdn.net/images/misc/5c27115d3b797954afff59199dad98d1/250x250-000000-80-0-0.jpg",
      "albumDeezerId": 43485811,
      "albumName": "Grateful",
      "albumCoverMedium": "https://cdns-images.dzcdn.net/images/cover/31c5c7f955540f34d0f109be6e86bc19/250x250-000000-80-0-0.jpg",
      "artistDeezerId": 5828,
      "artistName": "DJ Khaled",
      "artistPictureMedium": "https://cdns-images.dzcdn.net/images/artist/e12823240d118f66a93890141de670dd/250x250-000000-80-0-0.jpg"
      },
      {
      "id": 270,
      "trackDeezerId": 1372275252,
      "title": "1 0 0 . m i l ‘",
      "duration": 163,
      "rank": 951016,
      "previewUrl": "https://cdns-preview-a.dzcdn.net/stream/c-a325f3cd2efa968752e24283c9d14a61-4.mp3",
      "genreDeezerId": 116,
      "genreName": "Rap/Hip Hop",
      "genrePictureMedium": "https://cdns-images.dzcdn.net/images/misc/5c27115d3b797954afff59199dad98d1/250x250-000000-80-0-0.jpg",
      "albumDeezerId": 230155192,
      "albumName": "The Off-Season",
      "albumCoverMedium": "https://cdns-images.dzcdn.net/images/cover/1956b602e48e7d0cc9898a0288446234/250x250-000000-80-0-0.jpg",
      "artistDeezerId": 339209,
      "artistName": "J. Cole",
      "artistPictureMedium": "https://cdns-images.dzcdn.net/images/artist/48f25bd8beddbc4c07332ab8cab29317/250x250-000000-80-0-0.jpg"
      },
      {
      "id": 556,
      "trackDeezerId": 1024026262,
      "title": "10gb (feat. Bobkata & Red Brodie)",
      "duration": 213,
      "rank": 100585,
      "previewUrl": "https://cdns-preview-b.dzcdn.net/stream/c-badb87e0bc856a448b8b6554488fa619-2.mp3",
      "genreDeezerId": 116,
      "genreName": "Rap/Hip Hop",
      "genrePictureMedium": "https://cdns-images.dzcdn.net/images/misc/5c27115d3b797954afff59199dad98d1/250x250-000000-80-0-0.jpg",
      "albumDeezerId": 161292422,
      "albumName": "Perfectum",
      "albumCoverMedium": "https://cdns-images.dzcdn.net/images/cover/1ed27be81ef16984ca80310aef5beefd/250x250-000000-80-0-0.jpg",
      "artistDeezerId": 3209251,
      "artistName": "Marso",
      "artistPictureMedium": "https://cdns-images.dzcdn.net/images/artist/f8ce5d3f4f8eb497763ac441f9fe912a/250x250-000000-80-0-0.jpg"
      }
    ]
  ```

- GET /v1/api/tracks/:id

  - Get a single track by track id.
  - Example request and response:

  Request:

  ```
    http://localhost:5000/v1/api/tracks/1
  ```

  Response:

  ```
    {
      "id": 1,
      "trackDeezerId": 1182496892,
      "title": "Alfred (Intro)",
      "duration": 17,
      "rank": 175148,
      "previewUrl": "https://cdns-preview-d.dzcdn.net/stream/c-db33f2d813db537edf37a1da711f70d7-4.mp3",
      "genreDeezerId": 116,
      "genreTitle": "Rap/Hip Hop",
      "genrePictureMedium": "https://cdns-images.dzcdn.net/images/misc/5c27115d3b797954afff59199dad98d1/250x250-000000-80-0-0.jpg",
      "albumDeezerId": 194219042,
      "albumTitle": "Music To Be Murdered By - Side B (Deluxe Edition)",
      "albumCoverMedium": "https://cdns-images.dzcdn.net/images/cover/55d424a53904d145b17869ba133843d7/250x250-000000-80-0-0.jpg",
      "artistDeezerId": 13,
      "artistName": "Eminem",
      "artistPictureMedium": "https://cdns-images.dzcdn.net/images/artist/0707267475580b1b82f4da20a1b295c6/250x250-000000-80-0-0.jpg"
    }
  ```

- POST /v1/api/users/playlists

  - Create new playlist for the user making the request.
  - Example body:

  ```
  {
    "name": "Example playlist name",
    "tracks": [...deezerIds],
    "playtime": "12345",
    "genres": [...deezerIds],
    "poster": "https://cdns-images.dzcdn.net/images/artist/0707267475580b1b82f4da20a1b295c6/250x250-000000-80-0-0.jpg"
  }
  ```

- PUT /v1/api/users

  - Update user's username or password. This is applied to the user making the request.
  - Example request body:

  ```
    {
      "password": "12345678"
    }
  ```

- PUT /v1/api/users/playlists/:pid

  - Update a single playlist's name. The playlist must be created by the user making the request.
  - Example request:

  ```
  {
    "name": "New playlist name"
  }
  ```

#### Admins only

- GET /v1/api/admin/users

  - Get all users.
  - Returns an array with user objects.

- PUT /v1/api/admin/users/:id

  - Update user's information by user id. This can be used for updating username, password, email, deleting user, promoting user's rank (User/Admin).
  - Example request body:

  ```
    {
      "username": "new_username",
      "password": "12345678",
    }
  ```

- PUT /v1/api/admin/banstatus/:id

  - Ban/Remove ban of a user by user id.
  - Example request body:

  ```
  {
    "message": "Success: user 1 was banned until 10-10-2021 09:15:30"
  }
  ```

- PUT /v1/api/admin/users/:id/playlists/:pid

  - Update a single playlist's name of a user by user and playlist ids.
  - Example body: same as user's playlist update.

- DELETE /v1/api/admin/users/:id/playlists

  - Delete all playlists of a user by user id.

- DELETE /v1/api/admin/users/:id/playlists/:pid

  - Delete a single playlist of a user by user and playlist ids.
